---
title: "Writing Literate Configs with Org-mode"
author: ["Jacob Hilker"]
date: 2022-02-12T00:00:00-05:00
slug: "literate-dotfiles-orgmode"
tags: ["Literate-Programming", "Emacs", "Org-mode"]
categories: ["Emacs", "Org-mode"]
type: "post"
draft: true
featured: false
enableToc: false
---

One of the features I have come to love about org-mode is the ability to extract source code to a particular file with `org-babel`. What this means is that instead of having to write both separate documentation and code for a particular project, I simply need to write one "file" that contains both the documentation AND the source code for my configuration. This is especially helpful for teaching someone programming, since instead of needing a Jupyter notebook (or Google Colab), you simply need Emacs - not even an internet connection (at least if you already have it set up).

Although I know [DistroTube](https:youtube.com/watch?v=pQe1ul51RM0) has made a video on this previously, I feel like anything that could help people get into Emacs as a program is a plus, especially because not only is it free and open-source but also because of just how extensible it is.

Anyway, the main thing you will need to add to your literate config is this line:

```org
#+property: header_args:lang :tangle path/to/your/config
```