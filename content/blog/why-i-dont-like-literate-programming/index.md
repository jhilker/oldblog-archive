---
title: "Why I Don't Like Literate Programming"
author: ["Jacob Hilker"]
date: 2021-11-28T10:41:00-05:00
slug: "why-i-dont-like-literate-programming"
tags: ["Literate-Programming", "Emacs", "Orgmode"]
categories: ["Emacs", "Orgmode"]
type: "post"
draft: true
featured: "%^{Featured?|false|true}"
enableToc: false
---
