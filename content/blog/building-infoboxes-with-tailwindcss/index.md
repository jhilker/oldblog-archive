---
title: "Building Infoboxes with TailwindCSS"
author: ["Jacob Hilker"]
date: 2021-11-21T12:31:00-05:00
slug: "building-infoboxes-with-tailwindcss"
type: "post"
draft: true
featured: false
enableToc: false
series: "Building Wikis with Hugo and Org-Roam"
---

Arguably the most important part of a wiki