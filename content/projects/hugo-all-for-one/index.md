---
title: "All for One"
author: ["Jacob Hilker"]
type: "project"
draft: false
weight: 2
featured: true
status: "Available Now"
repo:
  link: "https://github.com/jhilker1/hugo-all-for-one"
  icon: "devicon-github-plain"
  name: "Github"
---

All for One is a Hugo theme built with [Halfmoon CSS](https://github.com/halfmoonui/halfmoon) that covers several uses for a personal website - a blog, portfolio, and CV.