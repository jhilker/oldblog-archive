---
title: "Full-Stack Engineer"
author: ["Jacob Hilker"]
layout: "resume"
draft: false
skills: ["Python", "Shell Scripts (Bash/ZSH)", "Java", "HUGO/CSS", "JavaScript", "PostgreSQL", "Org-mode", "Groff"]
soft_skills: ["Problem Solving", "Critical Thinking", "Creativity", "Adaptability"]
interests: ["Audio Engineering", "Sound Design", "Worldbuilding", "Songwriting"]
title: "Resumé"
---

<div class="flex justify-between mb-1.5">
<li class="list-none"><i class="fas fa-home"></i>&nbsp;&nbsp;Crozet, VA</li>
<li class="list-none"><i class="fas fa-mobile-alt"></i>&nbsp;&nbsp;434-409-3789</li>
<li class="list-none ">
        <a href="mailto:jacob.hilker2@gmail.com" class="!no-underline !text-slate-700 dark:!text-slate-300 hover:!text-royal-600 dark:hover:!text-royal-400"><span class="mr-3"><i class="fas fa-envelope"></i>&nbsp;&nbsp;jacob.hilker2@gmail.com</span></li></a>
</div>

## Profile {#profile}

<div class="cvwrapper">

Inquisitive, energetic software developer pursuing a position in software development. Skilled in multiple languages, but especially enjoys writing in Python.

</div>


## Education {#education}

<div class="cvwrapper">



{{<cventry title="B.Sc., Computer Science" start="2017-08-22" end="2021-05-09" employer="University of Mary Washington" location="Fredericksburg, VA">}}
**Minor in Cybersecurity**

**Dean's List, Spring 2021**

**<span class="underline">Relevant Coursework</span>**

-   Applications of Databases
-   Data Science
-   Artificial Neural Networks
-   Software Security

</div>


{{</cventry>}}


## Experience {#experience}


### Work {#work}

<div class="cvwrapper">



{{<cventry title="Freelance Web Developer" start="2022-01-28" end="" employer="Freelance" location="Remote">}}
-   Assisted a philosophy student with deploying a blog using [Ox-hugo](https://github.com/kashualmodi/ox-hugo).
-   Designed and deployed static websites for multiple clients using the Hugo static site generator.

</div>


{{</cventry>}}


### Volunteer {#volunteer}

<div class="cvwrapper">



{{<cventry title="Library Volunteer" start="2014-07-14" end="2017-07-17" employer="Crozet Library" location="Crozet, VA">}}
-   Shelved books by subject according to Dewey Decimal System.
-   Performed basic book reshelving and obtained items from book drop.
-   Assembled requested library and loaned materials for users.


{{</cventry>}}



{{<cventry title="Impact Richmond" start="2012-07-07" end="2017-07-15" employer="Impact Richmond" location="Richmond, VA">}}
-   Participated in week-long volunteer sessions to refurbish homes in disadvantaged neighborhoods in Richmond, VA.

</div>


{{</cventry>}}
